package lu.lns.connector.odoo;

import org.identityconnectors.common.security.GuardedString;

public class TestConnectorFactory {

    public OdooConnector getOdooConnector() {
        return getOdooConnector(null);
    }

    public OdooConnector getOdooConnector(String liveSyncModels) {
        OdooConfiguration cfg = new OdooConfiguration() {{
            setUrl("http://odoo:8069");
            setDatabase("db1");
            setUsername("admin");
            setPassword(new GuardedString("admin".toCharArray()));
            setLiveSyncModels(liveSyncModels);
        }};

        OdooConnector connector = new OdooConnector();
        connector.init(cfg);
        return connector;
    }

}
